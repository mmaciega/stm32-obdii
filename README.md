# STM32 OBDII #

Projekt wykorzystujący standard OBDII do obsługi diagnostyki samochodowej. Odczytywaniu parametrów z (wirtualnego) komputera pokładowego samochodu, obliczenie potrzebnych wartość parametrycznych według formuł (kodów) OBDII- PIDs. Na koniec wypisywany jest opis danych parametrów zawartych w pojeździe

## Schemat projektu ##

![Przechwytywanie.PNG](https://bitbucket.org/repo/ggGrb6/images/2201130248-Przechwytywanie.PNG)

- CrossWorks for ARM - zintegrowane środowisko programstyczne
- Docklight- narzędzie do testowania, analizy i symulacji protokoły komunikacji szeregowej (RS232, RS485/422 i inne). Pozwala na monitorowanie komunikacji pomiędzy dwoma urządzeniami szeregowymi lub do testowania komunikacji szeregowej z jednym urządzeniu. Docklight znacznie zwiększa wydajność w szerokim zakresie branż, w tym automatyzacji i kontroli, łączności motoryzacja,producenci sprzętu i ćwiczeniowe / produktów konsumenckich.

## Linki ##

Filmik przedstawiający działanie programu DockLight podczas komunikacji po między STM32 a komputerem - [link](https://youtu.be/vO4jcokUOVQ)