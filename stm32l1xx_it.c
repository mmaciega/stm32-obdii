/**
  ******************************************************************************
  * @file    Project/STM32L1xx_StdPeriph_Template/stm32l1xx_it.c 
  * @author  MCD Application Team
  * @version V1.0.0
  * @date    31-December-2010
  * @brief   Main Interrupt Service Routines.
  *          This file provides template for all exceptions handler and 
  *          peripherals interrupt service routine.
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2010 STMicroelectronics</center></h2>
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "stm32l1xx_it.h"
//#include "discover_board.h"
/* #include "main.h" */

/** @addtogroup Template_Project
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
typedef enum  {FALSE = 0,TRUE}bool;
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
unsigned char l = 0;
bool flaga_keep_alive = FALSE;
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/******************************************************************************/
/*            Cortex-M3 Processor Exceptions Handlers                         */
/******************************************************************************/

/**
  * @brief  This function handles NMI exception.
  * @param  None
  * @retval None
  */
void NMI_Handler(void)
{
}

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
void HardFault_Handler(void)
{
  /* Go to infinite loop when Hard Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval None
  */
void MemManage_Handler(void)
{
  /* Go to infinite loop when Memory Manage exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval None
  */
void BusFault_Handler(void)
{
  /* Go to infinite loop when Bus Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval None
  */
void UsageFault_Handler(void)
{
  /* Go to infinite loop when Usage Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles SVCall exception.
  * @param  None
  * @retval None
  */
void SVC_Handler(void)
{
}

/**
  * @brief  This function handles Debug Monitor exception.
  * @param  None
  * @retval None
  */
void DebugMon_Handler(void)
{
}

/**
  * @brief  This function handles PendSVC exception.
  * @param  None
  * @retval None
  */
void PendSV_Handler(void)
{
}



/******************************************************************************/
/*                 STM32L1xx Peripherals Interrupt Handlers                   */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_stm32l1xx_md.s).                                            */
/******************************************************************************/

/**
  * @brief  Przerwanie SysTick, wykonywane co 1ms. Funkcja za pomoca flagi czy_sygnal_keep_alive sygnalizuje nam ze czas bezczynnosci wynosi ok. 5s i ze nalezy wyslac sygnal podtrzymujacy polaczenie
  * @param  None
  * @retval None
  */
void SysTick_Handler(void)
{
	extern volatile bool czy_sygnal_keep_alive;
	extern volatile bool flaga_start_transmisji;
	extern volatile bool wyzeruj_licznik;
	static short int SYSTICK_Tick = 0;

	if(flaga_start_transmisji)                                                          //czas odliczany tylko po nawiazaniu polaczenia
	{
		SYSTICK_Tick++;                                                             //co 1 ms zwiekszamy wartosc zmiennej

	 if (SYSTICK_Tick == 5000)                                                          // minelo 5 sek
	 {
		 czy_sygnal_keep_alive = TRUE;                                              // dajemy znac programowi glownemu, ze minela sekunda
		 SYSTICK_Tick=0;
	 }
	}

	if(wyzeruj_licznik)                                                                 //sprawdzamy czy aby nie nalezy wyzerowac licznika poniewaz zostalo wyslane zapytanie o parametry
	{
		SYSTICK_Tick = 0;                                                           //zerujemy nasz licznik
		wyzeruj_licznik = FALSE;
	}


  /*  TimingDelay_Decrement(); */
  TimingDelay_Decrement();
}

/**
  * @brief  This function handles USART1_IRQ Handler.
  * @param  None
  * @retval None
  */
void USART1_IRQHandler(void)		// przerwanie USART1
{
	extern volatile int nr;
	extern volatile bool odebrano_informacje;
	extern volatile bool flaga_odebrania_ramek;
        extern volatile bool flaga_odebrania_formul_main;
        extern unsigned int liczba_wszystkich_ramek;
	extern volatile unsigned char **ramki;
	extern volatile unsigned char **formuly;
        extern volatile unsigned char **opisy_parametrow;
	extern volatile unsigned char snbuf[255];
	extern volatile unsigned char liczba_bajtow_ramki;

	static bool flaga_odebrania_komunikatow;
        static bool flaga_odebrania_formul;
	unsigned char pobrany_bajt;

	if(USART_GetFlagStatus(USART1, USART_FLAG_RXNE) != RESET)	// sprawdzamy czy przerwanie nast�pi�o z powodu zape�nienia buforu odczytu
	{
		pobrany_bajt = USART_ReceiveData(USART1); // zapis odebranego bajtu

		if(l == 0)
		{
			int liczba_bajtow_danych;
                         
                         //rozgraniczenie na wyliczanie liczby bajtow danych ( w ramkach ->, z formulami liczba ta wynosi pierwszy bajt - 0x80, w ramce z opisami jest ona po prostu pierwszym bajtem w ramce)
                        if(flaga_odebrania_formul == TRUE && flaga_odebrania_ramek == FALSE)
                          liczba_bajtow_danych = (int) pobrany_bajt;                                    //sprawdzamy ile mamy bajt�w danych, informacja ta zapisana jest w pierwszym bajcie ramki (ramka z opisami)
			else
                          liczba_bajtow_danych = (int) pobrany_bajt - (int) 0x80;			//sprawdzamy ile mamy bajt�w danych, informacja ta zapisana jest w pierwszym bajcie ramki - 0x80 (ramka -> i z formulami)
                        
                        liczba_bajtow_ramki = liczba_bajtow_danych + 4;					//dodajemy jeszcze cztery bajty, gdyz pierwsze trzy bajty ramki to:
		}

		snbuf[l] = pobrany_bajt;                                                                //zapisujemy bajt do bufora z programu glownego

		if(++l == liczba_bajtow_ramki)                                                          //sprawdzamy, czy odebralismy wszystkie bajty ramki
		{
			l = 0;

                        if(nr == -1)
                        {
                                liczba_wszystkich_ramek = snbuf[4];                                     //liczba wszystkich ramek jakie zostana przeslanie z PC do STM32 zapisana jest w pierwszej otrzymanej ramce na pozycji 4 po literze R
                                nr = 0;
                        }
			else if(flaga_odebrania_ramek == FALSE)                                              //sprawdzamy czy odebrana ramki jest jeszcze ramka konfiguracyjna
			{
                                //zapisujemy ramki ->
				if (flaga_odebrania_komunikatow == FALSE)                               //sprawdzamy czy odbieramy jeszcze ramki -> czy juz formuly do parametrow
				{
					int k;
					if((nr == 3) && (flaga_keep_alive == FALSE))                    //sprawdzamy czy odebrana ramka nie jest ramka keepalive
					{

                                                if ((ramki[0] = (unsigned char *) malloc ( liczba_bajtow_ramki * sizeof (unsigned char) )) == 0)
                                                    Delay(2000);  

						for(k=0; k< liczba_bajtow_ramki; k++)
							ramki[0][k] = snbuf[k];                         //zapisujemy odebrana ramke keepalive na pozycje 0 w tabeli

                                                nr--;
						flaga_keep_alive = TRUE;                                //sygnalizujemy, ze zostala juz odebrana ramka keepalive z PC
					}
					else                                                        
					{
                                                if ((ramki[nr+1] = (unsigned char *) malloc ( liczba_bajtow_ramki * sizeof (unsigned char) )) == 0)
                                                    Delay(2000);  

						for(k=0; k< liczba_bajtow_ramki; k++)
							ramki[nr+1][k] = snbuf[k];                      //pozostale ramki zapisujemy w typowej kolejnosci
					}

                                        nr++;

					if(nr == (liczba_wszystkich_ramek-1))                          //sprawdzamy czy nie zostaly odebrane wszystkie ramki ->, oznaczaloby to, ze teraz nadejda ramki z formulami a je nalezy zapisac w innej tabeli
					{
						flaga_odebrania_komunikatow = TRUE;                    //sygnalziujemy odebranie ramek -> z PC
						nr = 0;
					}
				}
                                //zapisujemy formuly
				else if (flaga_odebrania_formul == FALSE)
				{
                                        //alokujemy pamiec potrzebna dla formul parametrow dla konkretnej odpowiedzi na zadanie o parametry
                                        if ((formuly[nr] = (unsigned char *) malloc ( liczba_bajtow_ramki * sizeof (unsigned char) )) == 0)
                                            Delay(2000);  

					int k;
					for(k=0; k < liczba_bajtow_ramki; k++)
						formuly[nr][k] = snbuf[k];

					nr++;
					if(nr == (liczba_wszystkich_ramek-8))
                                        {
                                                flaga_odebrania_formul = TRUE;                        //sygnalziujemy odebranie formul z PC
                                                flaga_odebrania_formul_main = TRUE;                   //sygnalizujemy programowi glownemu, ze nalezy wyslac proste o przeslanie opisow parametrow
                                                nr = 0;
                                        }
                                }
                                //zapisujemy opisy parametrow
                                else
                                {
                                        //alokujemy pamiec potrzebna dla opisow parametrow dla konkretnej odpowiedzi na zadanie o parametry
                                        if ((opisy_parametrow[nr] = (unsigned char *) malloc ( liczba_bajtow_ramki * sizeof (unsigned char) )) == 0)
                                            Delay(2000);  

					int k;
					for(k=0; k < liczba_bajtow_ramki; k++)
						opisy_parametrow[nr][k] = snbuf[k];

                                        nr++;

					if(nr == (liczba_wszystkich_ramek-8))
						flaga_odebrania_ramek = TRUE;                          //sygnalizujemy programowi glownemu, ze odebralismy wszystkie ramki konfiguracyjne ->, formuly i opisy parametrow, teraz STM bedzie oczekiwal na rozpoczecie transmisji poprzez sygnal START
                                }
			}
			else
			{
				if((odebrano_informacje == FALSE))              // sprawdzamy czy odebrana poprzednio ramka danych zosta�a obsluzona przez program g��wny
				{
					odebrano_informacje = TRUE;		// sygnalizujemy programowi glownemu, ze odebrana zostala ramka danych
				}
			}
		}

	}
}

/**
  * @brief  This function handles external interrupts generated by UserButton.
  * @param  None
  * @retval None
  */

void EXTI0_IRQHandler(void)
{
  extern volatile bool flaga_przycisk_uzytkownika;
  /* set UserButton Flag */
  flaga_przycisk_uzytkownika = TRUE;

  EXTI_ClearITPendingBit(EXTI_Line0);
}

/******************* (C) COPYRIGHT 2010 STMicroelectronics *****END OF FILE****/
