/* Includes */
#include <stddef.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "stm32l1xx.h"
#include "discover_board.h"

/* Private typedef */
typedef enum  {FALSE = 0,TRUE} bool;                            //definiujemy zmienna logiczna "bool"

/* Private define  */
/* Private macro */
/* Private variables */
RCC_ClocksTypeDef RCC_Clocks;

unsigned int liczba_wszystkich_ramek = 0;

volatile bool odebrano_informacje = FALSE;				//informuje, ze odebrano informacje przez USART
unsigned char snbuf[255];					//pomocniczy bufor, zapisujemy w nich odebrane ramki danych
unsigned char liczba_bajtow_ramki = 0;				//liczba bajt�w aktualnie odebranej ramki, ktora przechowywana jest w zmiennej snbuf
static volatile uint32_t TimingDelay;

volatile unsigned char **ramki = NULL;                            //zmienna gdzie zapisane sa wszystkie ramki -> przeslane przy konfiguracji z PC
volatile unsigned char **formuly;                                 //formuly oraz pozycje parametrow do odpowiedzi o parametry na podstawie ktorych odczytujemy wartosci parametrow Forma danych w ramce formuly: "P liczba_parametrow pozycja_parametru_nr1 ... F formula_par1 K formula_par2 K ..."
volatile unsigned char **opisy_parametrow;                        //tablica opisow do parametrow

volatile int nr = -1;                                               
volatile bool flaga_odebrania_ramek = FALSE;                             //flaga sygnalizujaca, ze ramki konfiguracyjne oraz formuly zostaly odebrane
volatile bool flaga_odebrania_formul_main = FALSE;
volatile bool flaga_start_transmisji = FALSE;                            //flaga sygnalizujaca, ze odebrano ramke rozpoczynajaca komunikacje (ramka z dana START)
volatile bool czy_sygnal_keep_alive = FALSE;                             //flaga sygnalizujaca, ze zostal wyslany sygnal podtrzymania
volatile bool czekam_na_keep_alive = FALSE;                              //flaga sygnalizujaca, oczekiwanie na odpowiedz na sygnal podtrzymania
volatile bool wyzeruj_licznik = FALSE;                                   //flaga sygnalizujaca, ze nalezy wyzerowa licznik SysTick poniewaz wyslano jakis komunikat (nie trzeba podtrzymac sygnalu

volatile bool flaga_przycisk_uzytkownika;                       //flaga sygnalizujaca, ze uzytkownik wcisnal przycisk
volatile int nr_zapytania_o_parametr = 6;                                //nr indeksu w tabeli aktualnie do wyslania zapytania o parametry

/* Private function prototypes */
void Init_GPIOs(void);
void RCC_Configuration(void);
void RTC_Configuration(void);
void Config_USART(void);
void Interrupts_Configuration(void);
void Delay(uint32_t nTime);
void TimingDelay_Decrement(void);
void przeslij_ramke(int nr_ramki);                              //przeslanie ramki z tabeli do PC
char *pobierz_dane(int nr_ramki);                               //pobranie danych z ramki 
void wyswietl_parametry(int nr_zapytania);                      //wyswietlenie parametrow otrzymanych w odpowiedzi o parametry wyliczone na podstawie formulek

//funkcje wykorzystywane do wyliczania formuly
double obliczenie_formuly(char formula[], int wartosc_parametru);           //funkcja obliczajca wartosci parametrow zgodnie z formulami
double wynik_dzialania(double arg_wart1, double arg_wart2, char znak);      //funkcja obliczajca wynik dzialania matematycznych dla zadanych argumentow
bool czy_dzialanie(char c);                                                 //funkcja sprawdzajaca czy przekazany argument jest znak dzialania matematycznego
bool czy_liczba(char c);                                                    //funkcja sprawdzajaca czy przekazany argument jest liczba

/* Private functions */
 void Config_Systick()
 {
   RCC_GetClocksFreq(&RCC_Clocks);
   SysTick_Config(RCC_Clocks.HCLK_Frequency / 1000);		//przerwanie co 1ms
 }

/*
--------------				MAIN 			------------------------
*/
int main(void)
{
  /* --------- konfiguracja mikroprocesora --------- */
  RCC_Configuration();                          //konfiguracja RCC
  RTC_Configuration();                          //konfiguracja RTC
  Init_GPIOs ();				//zainicjowanie GPIO
  Config_USART();				//konfiguracja USART
  Interrupts_Configuration();                   //konfiguracaj przerwa� NVIC
  Config_Systick();				//konfiguracja przerwania Systick

  //wysylamy ramke inicjujaca polaczenie (docklight odpowiada ramkami ->,formulami i opisem parametrow)
  char ramka_inicjujaca[] = {0x86, 0x10, 0xF1, 0x43, 0x4F, 0x4E, 0x46, 0x49, 0x47, 0x3D, 0x0};
  USART_PutString(ramka_inicjujaca);

  //czekamy na odebranie pierwszej ramki z liczba ramek do odebrania z komputera 
  while(nr != 0);

  //rezerwujemy tyle wierszy w tabeli ile mamy otrzymac ramek od PC
  if((ramki = (unsigned char** ) malloc ( liczba_wszystkich_ramek * sizeof (unsigned char *) )) == 0)
  {
     Delay(2000);
  }

  //rezerwujemy tyle wierszy w tabeli z formulami ile mamy otrzymac zapytan o parametry z PC, -8 poniewaz mamy 5 ramek do inicjalizacji polaczenia oraz 2 do zakonczenia
  if((formuly = (unsigned char** ) malloc ( (liczba_wszystkich_ramek-8) * sizeof (unsigned char *) )) == 0)
  {
     Delay(2000);
  }

  //rezerwujemy tyle wierszy w tabeli z opisami parametrow ile mamy otrzymac zapytan o parametry z PC, -8 poniewaz mamy 5 ramek do inicjalizacji polaczenia oraz 2 do zakonczenia
  if((opisy_parametrow = (unsigned char** ) malloc ( (liczba_wszystkich_ramek-8) * sizeof (unsigned char *) )) == 0)
  {
     Delay(2000);
  }

  bool czy_wyslano_zadanie = FALSE;
  //czekamy na odebranie ramek -> i formu� z komputera
  while(flaga_odebrania_ramek == FALSE)
  {
      if((flaga_odebrania_formul_main == TRUE) && (czy_wyslano_zadanie == FALSE))
      {
        Delay(200);

        //wysylamy ramke zadajaca otrzymanie opisow parametrow (docklight odpowiada ramkami z opisem parametrow)
        char ramka_zadania_opisow[] = {0x84, 0x10, 0xF1, 0x44, 0x45, 0x53, 0x43, 0xA4, 0x0};

        int i;
        for(i=0; i<8; i++)
          USART_PutChar(ramka_zadania_opisow[i]);
      
        czy_wyslano_zadanie = TRUE;
      }
  }


  /* ------------- glowna petla programu -------------- */
  while (1)
  {
	  if(odebrano_informacje == TRUE)						//sprawdzamy czy zostaly odebrane dane z portu USART1
	  {
		  if(flaga_start_transmisji == FALSE)                                   //sprawdzamy czy otrzymalismy juz sygnal START rozpoczynajacy komunikacje
		  {
			  char *zmienna_z_danymi; 
			  zmienna_z_danymi = pobierz_dane(-1);                          //pobieramy dane do zmiennej aby sprawdzic co otrzymalismy

			  if(strcmp(zmienna_z_danymi, "START") == 0)                    //sprawdzamy czy otrzymalismy sygnal START
			  {
				  GPIO_HIGH(LD_PORT,LD_GREEN);				//sygnalizujemy odebranie ramki danych za pomoca zielonej lampki
				  Delay(200);
				  GPIO_LOW(LD_PORT,LD_GREEN);				//wylaczamy diode zielona

				  int i;
				  for(i = 1; i <6; i++)					//przesylamy ramki do inicjalizacji polaczenia
					  przeslij_ramke(i);

				  przeslij_ramke(0);
				  flaga_start_transmisji = TRUE;			//sygnalizujemy zainicjowanie polaczenia
			  }

			  free(zmienna_z_danymi);
		  }
                  //mamy juz nawiazane polaczenie
		  else
		  {
			  char *zmienna_z_danymi;
			  zmienna_z_danymi = pobierz_dane(-1);
			  if(strcmp(zmienna_z_danymi, "STOP") == 0)               //sprawdzamy czy otrzymalismy sygnal STOP
			  {
				  int i;
				  for(i = 16; i < liczba_wszystkich_ramek; i++)   //ponownie wysylamy ramki inicjujace
					  przeslij_ramke(i);                  

				  nr_zapytania_o_parametr = 6;                    //zapytania o parametry po wznowieniu polaczenia beda wysylane od poczatku (pozycja 6 w tabeli)
				  flaga_start_transmisji = FALSE;
			  }

			  free(zmienna_z_danymi);
		  }

		  czekam_na_keep_alive = FALSE;                                  //sygnalizujemy, ze juz nie czekamy na sygnal keep alive bo odebralismy
		  odebrano_informacje = FALSE;                                   //sygnalizujemy obsluzenie odebrania informacji
	  }
	  else if (czekam_na_keep_alive == TRUE)			//sprawdzamy czy STM oczekuje otrzymania sygnalu keep alive od urzadzenia
	  {
		  int i;
		  for(i = 1; i <6; i++)					//nie otrzymalismy odpowiedzi na sygnal keep alive, wysylamy ponownie sygnaly inicjalizacji polaczenia
			  przeslij_ramke(i);

		  przeslij_ramke(0);                                    //wyslanie sygnalu podtrzymania polaczenia
		  czekam_na_keep_alive = FALSE;                         
	  }


	  if((czy_sygnal_keep_alive) && (flaga_start_transmisji))   //sprawdzamy czy nalezy wyslac ramke do potrzymania (ok. 5 sek nieaktywnosci), robimy to jedynie wtedy kiedy mamy nawiazane polaczenie
	  {
		 przeslij_ramke(0);                                 //wysylamy ramkie podtrzymania polaczenia
		 czy_sygnal_keep_alive = FALSE;                     //zerujemy flage sygnalizujaca, ze nalezy wyslac sygnal podtrzymania
		 czekam_na_keep_alive = TRUE;                       //ustawiamy flage sygnalizujaca, oczekiwanie na otrzymanie odpowiedzi na sygnal podtrzymania
	  }

	  if (flaga_przycisk_uzytkownika == TRUE)                   //sprawdzamy czy zostal przycisniety przycisk uzytkownika
	  {
		  	flaga_przycisk_uzytkownika = FALSE;         //zerujemy flage sygnalizujaca, ze zostal wciesniety przycisk przez uzytkownika
			GPIO_HIGH(LD_PORT,LD_BLUE);                 //sygnalizujemy odebranie ramki danych za pomoca niebieskiej lampki
			Delay(300);
                        GPIO_LOW(LD_PORT,LD_BLUE);                  //wylaczamy diode niebieska

			if(flaga_start_transmisji)                  //sprawdzamy czy mamy nawiazane polaczenie, jesli mamy nalezy wyslac odpowiednie zapytanie o parametr
			{
				przeslij_ramke(nr_zapytania_o_parametr);  //przesylamy konkretna ramke z zapytaniem o parametr
				nr_zapytania_o_parametr++;

                                wyzeruj_licznik = TRUE;                   //wyslalismy komunikat, zerujemy odliczanie do wyslania sygnalu podtrzymania komunikacji
				czekam_na_keep_alive = TRUE;              //sygnalizujemy, ze czekamy na jakas odpowiedz od PC

				Delay(100);

				wyswietl_parametry((nr_zapytania_o_parametr-1) - 6);      //wyswietlamy parametry dla otrzymanej odpowiedzi na zapytanie o parametry
         
                                  if(nr_zapytania_o_parametr == 16)                       //sprawdzamy czy wyslalismy wszystkie zapytania o parametr z tabeli
					nr_zapytania_o_parametr = 6;                      //wracamy z indeksem na poczatek naszej listy zapytan o parametry
			}
	  }

	  Delay(100);
  }

return 0;
}


/**
  * @brief  Configures the different system clocks.
  * @param  None
  * @retval None
  */
void RCC_Configuration(void)
{
  /* Enable the GPIOs Clock */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA | RCC_AHBPeriph_GPIOB | RCC_AHBPeriph_GPIOC| RCC_AHBPeriph_GPIOD| RCC_AHBPeriph_GPIOE| RCC_AHBPeriph_GPIOH, ENABLE);

  /* Enable comparator clock PWR */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR,ENABLE);

  /* Enable SYSCFG, USART1*/
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG | RCC_APB2Periph_USART1, ENABLE);
}

/**
  * @brief  Configures the real-time clock.
  * @param  None
  * @retval None
  */
void RTC_Configuration(void)
{
     /* Allow access to the RTC */
      PWR_RTCAccessCmd(ENABLE);

      /* Reset Backup Domain */
      RCC_RTCResetCmd(ENABLE);
      RCC_RTCResetCmd(DISABLE);

      /*! LSE Enable */
      RCC_LSEConfig(RCC_LSE_ON);

      /*! Wait till LSE is ready */
      while (RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET)
      {}

      /*! LCD Clock Source Selection */
      RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);
}

/**
  * @brief  To initialize the I/O ports
  * @caller main
  * @param None
  * @retval None
  */

void  Init_GPIOs (void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;

	/*	-----------------	USART1	-----------------------  */
    //Konfiguracja PA10 jako Rx
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_Init( GPIOA, &GPIO_InitStructure);
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource10,GPIO_AF_USART1) ;

    //Konfiguracja PA9 jako Tx
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_Init( GPIOA, &GPIO_InitStructure);
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource9,GPIO_AF_USART1) ;

    //Konfiguracja przycisku
    GPIO_InitStructure.GPIO_Pin = USER_GPIO_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_Init(BUTTON_GPIO_PORT, &GPIO_InitStructure);

    //Konfiguracja diod LED
    GPIO_InitStructure.GPIO_Pin = LD_GREEN|LD_BLUE;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_Init(LD_PORT, &GPIO_InitStructure);
    GPIO_LOW(LD_PORT,LD_GREEN);
    GPIO_LOW(LD_PORT,LD_BLUE);
}

/**
  * @brief  Configures the USART.
  * @param  None
  * @retval None
  */
void Config_USART()
{
    // Konfiguracja USART1
    USART_InitTypeDef USART_InitStructure;

    USART_InitStructure.USART_BaudRate = 9600;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
    USART_Init(USART1, &USART_InitStructure);

    USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
    USART_ITConfig(USART1, USART_IT_TXE, DISABLE);
    USART_Cmd(USART1,ENABLE);
}

/**
  * @brief  Configures the interrupts.
  * @param  None
  * @retval None
  */
void Interrupts_Configuration()
{
	#ifdef  VECT_TAB_RAM
		// Jezeli tablica wektorow w RAM, to ustaw jej adres na 0x20000000
		NVIC_SetVectorTable(NVIC_VectTab_RAM, 0x0);
	#else  // VECT_TAB_FLASH
		// W przeciwnym wypadku ustaw na 0x08000000
		NVIC_SetVectorTable(NVIC_VectTab_FLASH, 0x0);
	#endif

	EXTI_InitTypeDef EXTI_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	/* Connect Button EXTI Line to Button GPIO Pin */
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA,EXTI_PinSource0);

	/* Configure User Button and IDD_WakeUP EXTI line */
	EXTI_InitStructure.EXTI_Line = EXTI_Line0 ;  // PA0 for User button AND IDD_WakeUP
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);

	/* Enable and set User Button and IDD_WakeUP EXTI Interrupt to the lowest priority */
	NVIC_InitStructure.NVIC_IRQChannel = EXTI0_IRQn ;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0F;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0F;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;

	NVIC_Init(&NVIC_InitStructure);
}

/**
  * @brief  Inserts a delay time.
  * @param  nTime: specifies the delay time length, in ms.
  * @retval None
  */
void Delay(uint32_t nTime)
{
  TimingDelay = nTime;

  while(TimingDelay != 0);

}

/**
  * @brief  Decrements the TimingDelay variable.
  * @param  None
  * @retval None
  */
void TimingDelay_Decrement(void)
{
  if (TimingDelay != 0x00)
  {
    TimingDelay--;
  }
}

/**
  * @brief  Funkcja za pomoca, ktorej przesylamy informacje do PC
  * @param  nr_ramki : Nr. ramki w tabeli, ktora chcemy przeslac do komputera. Je�li -1 to przesylamy ramke znajduj�ca si� w buforze odbiorczym.
  * @retval None
  */
void przeslij_ramke(int nr_ramki)
{
	if(nr_ramki == -1)
	{
		  int liczba_bajtow = ((int) snbuf[0] - (int) 0x80) + 4;
		  int j;
		  for(j=0; j<liczba_bajtow; j++)
			  USART_PutChar(snbuf[j]);
	}
	else
	{
		  int liczba_bajtow = ((int) ramki[nr_ramki][0] - (int) 0x80) + 4;
		  int j;
		  for(j=0; j<liczba_bajtow; j++)
			  USART_PutChar(ramki[nr_ramki][j]);
	}
}


/**
  * @brief  Funkcja za pomoca, ktorej pobieramy dane z bufora odbiorczego
  * @param  None
  * @retval None
  */
char* pobierz_dane(int nr_ramki)
{
		char liczba_bajtow_danych = ((int) snbuf[0] - (int) 0x80);

		char zmienna_z_danymi[liczba_bajtow_danych];

		int i, j = 0;
		for(i = 3; i<liczba_bajtow_danych+3; i++)
		{
			zmienna_z_danymi[j] = snbuf[i];
			j++;
		}

		zmienna_z_danymi[j] = '\0';

		return zmienna_z_danymi;
}

/**
  * @brief  Przeslanie pojedynczego bajtu przez port USART1
  * @param  value : Znak, ktory przesylamy poprzez port USART1
  * @retval None
  */
void USART_PutChar(char value)
{
	USART_SendData(USART1, value);
	while(USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);
}

/**
  * @brief  Przeslanie lancucha bajtow przez port USART1
  * @param  *s : Adres lancucha bajtow, ktory chcemy przeslac przez port USART1
  * @retval None
  */
void USART_PutString(char * s)
{
	while(*s != '\0')
		USART_PutChar(*s++);
}

/**
  * @brief  Funkcja za pomoca, ktorej wyswietlamy wszystkie parametry otrzymane w odpowiedzi wykorzystujaca odpowiednia formule
  * @param  nr_zapytania : Nr. zapytania w tabeli o parametr, na ktore otrzymalismy odpowiedz.
  * @retval None
  */
void wyswietl_parametry(int nr_zapytania)
{
	int liczba_parametrow = (int) formuly[nr_zapytania][4];                         //odczytujemy z ramki z formula z pozycji 4 liczbe parametrow w danej odpowiedzi
	int indeks_formuly = 5 + liczba_parametrow;                                     //pozycja od ktorej zaczynaja sie formuly na kolejnych parametrow
        int indeks_opisu = 3;

	int i;
	for(i = 1; i<=liczba_parametrow; i++)						//odczytujemy wszystkie parametry dla konkretnego komunikatu od komputera
	{
		int wartosc_parametru = (int) snbuf[formuly[nr_zapytania][4+i]-1];      //zapisujemy do zmiennej wartosc parametru odpowiedzi. Nr. pozycji parametru znajduje sie w ramce z formulami

                indeks_formuly++;                                                       //przechodzimy przez literke F oznaczajaca rozpoczenie formul dla parametrow		
                indeks_opisu++;                                                         //przechodzimy przez literke O oznaczajaca rozpoczenie opisow dla parametrow		
      
		char formula[120];
                char opis[100];
		int j = 0, k = 0;

                //wyciagniecie formuly dla konkretnego parametru
		while(formuly[nr_zapytania][indeks_formuly] != 'K')                     //dopoki nie trafimy na koniec formuly zapisujemy ja do zmiennej tymczasowej
		{
			formula[j] = formuly[nr_zapytania][indeks_formuly];
			indeks_formuly++; j++;
		}

		formula[j] = '\0'; 

                //wyciagniecie opisu dla konkretnego parametru
		while(opisy_parametrow[nr_zapytania][indeks_opisu] != 'K')                     //dopoki nie trafimy na koniec opisu zapisujemy ja do zmiennej tymczasowej
		{
			opis[k] = opisy_parametrow[nr_zapytania][indeks_opisu];
			indeks_opisu++; k++;
		}

		opis[k] = '\0';

		double wartosc_formuly = obliczenie_formuly(formula, wartosc_parametru);  //funkcja zwraca nam wyliczona zgodnie z formula wartosc i-tego parametu

		char bufor[400];
		sprintf(bufor, "Parametr %d.%d: %s, wartosc: %lf\n", nr_zapytania + 1, i, opis, wartosc_formuly);   //zamiana wartosci liczbowych na lancuch
	
                USART_PutString(bufor);                                                 //przeslanie wartosci parametrow do PC
        }
}

/**
  * @brief  Funkcja za pomoca wyliczamy wartosc parametru zgodnie z przekazana formula
  * @param  formula : Formula dla parametru w formie lancucha
            wartos_parametru : Wartosc parametru, ktora zostanie uzyta w formule
  * @retval Zwraca wyliczona wartosc formuly
  */
double obliczenie_formuly(char formula[], int wartosc_parametru)
{
	//bufor gdzie bedziemy przechowywac kopie formuly
	char buffor[50];

	strcpy(buffor, formula);

	//indeksy dla formuly i argumentow dzialan matematycznych
	int indeks = 0, indeks_arg = 0;

	//tablica przechowujace dwa argumenty dla dzialan matematycznych
	double argumenty[2];
	char znak = '\0';

	//zmienna przechowujaca wynik formul
	double wynik;

	while(formula[indeks] != '\0')			//przechodzimy przez wszystkie znaki formuly
	{
		int przesuniecie = 1;				//liczba oznaczajaca dlugosc jednego z argumentu lub znaku dzialania o ktora nalezy przesunac indeks dla formuly

		if(formula[indeks] == 'A')			//sprawdzamy czy dany znak jest wartoscia parametru
		{
			strcpy(buffor, formula+indeks+1);				//kopiujemy do bufora wszystkie znaki znajdujace sie po A

			argumenty[indeks_arg] = wartosc_parametru;		//naszym argumentem jest wartosc parametru
			
                        if((buffor[0] == '\0') && (znak == '\0'))
                          return argumenty[indeks_arg];
                        
                        indeks_arg++;									//zwiekszamy indeks dla argumentow
		}
		else if(czy_dzialanie(formula[indeks]))                       //sprawdzamy czy aktualny znak w formule nie jest operatorem dzialania matematycznego
		{
			strcpy(buffor, formula+indeks+1);                     //kopiujemy lancuch formuly do bufora od pozycji za znakiem

			znak = formula[indeks];                              //zapisujemy operator dzialania matematycznego
		}
		else if (formula[indeks] == '(' || formula[indeks] == ')')  //sprawdzamy czy nie trafilismy na nawiasy
		{
			strcpy(buffor, formula+indeks+1);                   //przesuwamy nasz bufor o jeden
		}
		else                                                        //mamy do czynienia z liczba
		{
			sscanf(buffor, "%lf", &argumenty[indeks_arg]);      //zapisujemy wartosc do tabeli argumentow dla dzialania matematycznego

			przesuniecie = 0;                                   //wartosc zmiennej oznaczaja ile bajtow zajmuje aktulna wartosc liczbowa w lancuchu formula

  
			while (czy_liczba(buffor[przesuniecie]))     //przesuwamy wskaznik po za liczbe
			{
				przesuniecie++; 
			}

			strcpy(buffor, buffor+przesuniecie);                //przesuwamy bufor o pozycje za liczbe

			indeks_arg++;                                       
		}


		if(indeks_arg == 2)                                         //jesli mamy zapisane dwa argumenty oznacza to ze musimy wykonac dzialanie matematyczne
		{
			wynik = wynik_dzialania(argumenty[0], argumenty[1], znak);    //funkcja zwraca nam wartosc rownania matematycznego dla podanych argumentow oraz operatora matematycznego
			argumenty[0] = wynik;                                         //zapisujemy wynik jako lewy argument dla kolejnych dzialan matematycznych
			indeks_arg = 1;                                               //na pozycji 0 w argumentach mamy zapisany wynik z poprzedniego dzialania matematycznego, teraz musimy odnalezc prawy (2) argument dzialania
		}

		indeks+=przesuniecie;                                      
	}

	return wynik;                                                     ///sprawdzilismy wszystkie znaki, zwracamy wynik operacji
}

/**
  * @brief  Wyliczamy wynik dzialania matematycznego dla podanych argumentow oraz zadanej operacji
  * @param  arg_wart1 : Wartosc pierwszego argumentu dla dzialania matematycznego
  * 		arg_wart2 : Wartosc drugiego argumentu dla dzialania matematycznego
  * 		znak : Znak operacji matematycznej
  * @retval Zwracany jest wynik operacji matematycznej
  */
double wynik_dzialania(double arg_wart1, double arg_wart2, char znak)
{
	double wynik;

        //sprawdzamy po kolei z jakims operatorem matematycznym mamy do czynienia i wykonujemy obliczenia
	switch(znak)
	{
	case '+':
		wynik = arg_wart1 + arg_wart2;
		break;

	case '-':
		wynik = arg_wart1 - arg_wart2;
		break;

	case '*':
		wynik = arg_wart1 * arg_wart2;
		break;

	case '/':
		wynik = (double) arg_wart1 / arg_wart2;
		break;

	case '%':
		wynik = (int) arg_wart1 % (int) arg_wart2;
		break;
	}

	return wynik;       //zwracamy wynik
}


/**
  * @brief  Funkcja sprawdzajaca czy przekazany znak jako arugment jest operatorem matematycznym
  * @param  c : Znak do sprawdzenia
  * @retval Wartosc logincza, true jesli wartosc jest operatorem matematycznym, w przeciwnym wypadku false
  */
bool czy_dzialanie(const char c)
{
    if (c == 0) return 0;
    return strchr("+-/*%", c) != 0;
}

/**
  * @brief  Funkcja sprawdzajaca czy przekazany znak jako arugment jest liczba
  * @param  c : Znak do sprawdzenia
  * @retval Wartosc logincza, true jesli wartosc jest liczba, w przeciwnym wypadku false
  */
bool czy_liczba(char c)
{
    if (c == 0) return 0;
    return strchr("0123456789", c) != 0;
}